/**
 * Created by Yogesh on 30-03-2016.
 */
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;
var startListening = true;
recognizeSpeech();
function recognizeSpeech() {
	var maxMatches = 1;
	var promptString = "Speak now"; // optional
	var language = "en-IN";                     // optional
	window.plugins.speechrecognizer.startRecognize(function(result){
		final_span.innerHTML = result;
		sendDataToAlchemy(result);
	}, function(errorMessage){
		console.log("Error message: " + errorMessage);
	}, maxMatches, promptString, language);
}
function checkFlorenceCall(transcript,currentValue) {
    if(!currentValue) {
        var called = transcript.search('Florence');
        if (called == -1)
            called = transcript.search('florence');
        if (called != -1)
        {
            responsiveVoice.speak("Hi. Lets get started!");
            return true;
        }
    }
    else {
        called = transcript.search('bye');
        if (called != -1)
            return false;
    }
    return currentValue;
}
function upgrade() {
    start_button.style.visibility = 'hidden';
}
var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
    return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}
var first_char = /\S/;
function capitalize(s) {
    return s.replace(first_char, function (m) {
        return m.toUpperCase();
    });
}
function startButton(event) {
    if (recognizing) {
        sendDataToAlchemy(final_span.innerHTML);
        recognition.stop();
        return;
    }
    final_transcript = '';
    recognition.start();
    ignore_onend = false;
    final_span.innerHTML = '';
    interim_span.innerHTML = '';
    start_img.src = 'mic-slash.gif';
    start_timestamp = event.timeStamp;
}
function sendDataToAlchemy(transcript){
    var url = 'http://www.showmemyday.com/Florence.php';
    $.ajax({
        type: "POST",
        url: url,
        data: { text:""+transcript } ,
        error:function(xhr, ajaxOptions, thrownError){
            console.log("error");
            complainsBox_span.innerHTML = "Error. "+thrownError;
            //responsiveVoice.speak("Error. "+thrownError);
        },
        success: function(data){
            var result = JSON.parse(data);
            result = result.result;
            setFlorenceResponse(result.fulfillment.speech);
            var actionName = result.action;
            final_transcript = "";
            window[actionName](result);
        },
        dataType: 'json'
    });
}
function setFlorenceResponse(speech){
    florenceResp_span.innerHTML = speech;
    //responsiveVoice.speak(speech);
	TTS
        .speak(speech, function () {
			recognizeSpeech();
        }, function (reason) {
            alert(reason);
        });
}
function setTreatment(result){
    var params = result.parameters;
    treatmentBox_span.innerHTML =  "Drug : "+params.med + "<br/>";
    treatmentBox_span.innerHTML += "No. of Doses : "+params.doses + "<br/>";
    treatmentBox_span.innerHTML += "Duration : "+params.days + " days<br/>";
    treatmentBox_span.innerHTML += "Before/after meals : "+params.medicine_before_after_meal + "<br/>";

}
function registerComplaint(result){
    complainsBox_span.innerHTML = ""+result.resolvedQuery;
    var params = result.parameters;
    setSymptoms(params);
}
function setSymptoms(params){
    symptomsBox_span.innerHTML =  "Symptom 1 : "+params.Illness + "<br/>";
    if(params.Illness1 != "")
        symptomsBox_span.innerHTML += "Symptom 2 : "+params.Illness1 + "<br/>";
    if(params.Illness2 != "")
        symptomsBox_span.innerHTML += "Symptom 3 : "+params.Illness2 + "<br/>";

}
function bookFollowUP(result){
    var params = result.parameters;
    followupBox_span.innerHTML = "Date : " +params.date+ "<br/> Time : " +params.time;

}