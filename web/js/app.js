/**
 * Created by Yogesh on 30-03-2016.
 */
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;
var startListening = false;
if (!('webkitSpeechRecognition' in window)) {
    upgrade();
} else {
    start_button.style.display = 'inline-block';
    var recognition = new webkitSpeechRecognition();
    recognition.continuous = true;
    recognition.interimResults = true;
    recognition.lang = 'en-IN';
    recognition.start();
    recognition.onstart = function () {
        recognizing = true;
        start_img.src = 'mic-animate.gif';
    };
    recognition.onerror = function (event) {
        if (event.error == 'no-speech') {
            start_img.src = 'mic.gif';
            ignore_onend = true;
        }
        if (event.error == 'audio-capture') {
            start_img.src = 'mic.gif';
            ignore_onend = true;
        }
        if (event.error == 'not-allowed') {
            if (event.timeStamp - start_timestamp < 100) {
            } else {
            }
            ignore_onend = true;
        }
    };
    recognition.onend = function () {
        recognizing = false;
        if (ignore_onend) {
            return;
        }
        start_img.src = 'mic.gif';
        if (!final_transcript) {
            return;
        }
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
            var range = document.createRange();
            range.selectNode(document.getElementById('final_span'));
            window.getSelection().addRange(range);
        }
    };
    recognition.onresult = function (event) {
            var interim_transcript = '';
            for (var i = event.resultIndex; i < event.results.length; ++i) {
                startListening = checkFlorenceCall(event.results[i][0].transcript,startListening);
                if(startListening) {
                    if (event.results[i].isFinal) {
                        final_transcript += event.results[i][0].transcript;
                        sendDataToAlchemy(final_transcript);
                    } else {
                        interim_transcript += event.results[i][0].transcript;
                    }
                }
            }
            final_transcript = capitalize(final_transcript);
            final_span.innerHTML = linebreak(final_transcript);
            interim_span.innerHTML = linebreak(interim_transcript);
    };
}
function checkFlorenceCall(transcript,currentValue) {
    if(!currentValue) {
        var called = transcript.search('Florence');
        if (called == -1)
            called = transcript.search('florence');
        if (called != -1)
        {
            responsiveVoice.speak("Hi. Lets get started!");
            return true;
        }
    }
    else {
        called = transcript.search('bye');
        if (called != -1)
            return false;
    }
    return currentValue;
}
function upgrade() {
    start_button.style.visibility = 'hidden';
}
var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
    return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}
var first_char = /\S/;
function capitalize(s) {
    return s.replace(first_char, function (m) {
        return m.toUpperCase();
    });
}
function startButton(event) {
    if (recognizing) {
        sendDataToAlchemy(final_span.innerHTML);
        recognition.stop();
        return;
    }
    final_transcript = '';
    recognition.start();
    ignore_onend = false;
    final_span.innerHTML = '';
    interim_span.innerHTML = '';
    start_img.src = 'mic-slash.gif';
    start_timestamp = event.timeStamp;
}
function sendDataToAlchemy(transcript){
    var url = 'http://www.showmemyday.com/Florence.php';
    $.ajax({
        type: "POST",
        url: url,
        data: { text:""+transcript } ,
		headers :{
			'Access-Control-Allow-Origin': '*'
		},
        error:function(xhr, ajaxOptions, thrownError){
            console.log("error");
            complainsBox_span.innerHTML = "Results receiving error. "+thrownError;
            //responsiveVoice.speak("Results receiving error. "+thrownError);
        },
        success: function(data){
            var result = JSON.parse(data);
            result = result.result;
            setFlorenceResponse(result.fulfillment.speech);
            var actionName = result.action;
            final_transcript = "";
            window[actionName](result);
        },
        dataType: 'json'
    });
}
function sendDataToApiAi(transcript){
    var url = 'https://api.api.ai/v1/query?v=20160405';
    $.ajax({
        type: "POST",
        url: url,
        data: '{ query:'+transcript+'lang:en,sessionId:"1234567890" }' ,
        headers: {
            'Authorization':'Bearer 511e7be6445f4555a2d393f979c0f631',
            'Content-Type':'application/json; charset=utf-8'
        },
        error:function(xhr, ajaxOptions, thrownError){
            console.log("error");
            complainsBox_span.innerHTML = "Results receiving error. "+thrownError;
            //responsiveVoice.speak("Results receiving error. "+thrownError);
        },
        success: function(data){
            var result = JSON.parse(data);
            result = result.result;
            setFlorenceResponse(result.fulfillment.speech);
            var actionName = result.action;
            final_transcript = "";
            window[actionName](result);
        },
        dataType: 'json'
    });
}
function setFlorenceResponse(speech){
    florenceResp_span.innerHTML = speech;
    responsiveVoice.speak(speech);
}
function setTreatment(result){
    var params = result.parameters;
    treatmentBox_span.innerHTML =  "Drug : "+params.med + "<br/>";
    treatmentBox_span.innerHTML += "No. of Doses : "+params.doses + "<br/>";
    treatmentBox_span.innerHTML += "Duration : "+params.days + " days<br/>";
    treatmentBox_span.innerHTML += "Before/after meals : "+params.medicine_before_after_meal + "<br/>";

}
function registerComplaint(result){
    complainsBox_span.innerHTML = "Results received. The complaint is : "+result.resolvedQuery;
    var params = result.parameters;
    setSymptoms(params);
}
function setSymptoms(params){
    symptomsBox_span.innerHTML =  "Symptom 1 : "+params.Illness + "<br/>";
    if(params.Illness1 != "")
        symptomsBox_span.innerHTML += "Symptom 2 : "+params.Illness1 + "<br/>";
    if(params.Illness2 != "")
        symptomsBox_span.innerHTML += "Symptom 3 : "+params.Illness2 + "<br/>";

}
function bookFollowUP(result){
    var params = result.parameters;
    followupBox_span.innerHTML = "Date : " +params.date+ "<br/> Time : " +params.time;

}